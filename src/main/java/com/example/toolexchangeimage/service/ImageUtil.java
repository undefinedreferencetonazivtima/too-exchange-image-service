package com.example.toolexchangeimage.service;

import com.example.toolexchangeimage.model.ImageFileExtension;

import java.util.Locale;
import java.util.UUID;

public class ImageUtil {
    public static String getImageFileName(UUID uuid, ImageFileExtension imageFileExtension) {
        return uuid.toString() + "." + imageFileExtension.name().toLowerCase(Locale.ROOT);
    }
}
