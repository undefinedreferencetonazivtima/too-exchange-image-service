package com.example.toolexchangeimage.service;

import com.example.toolexchangeimage.model.Image;
import com.example.toolexchangeimage.model.ImageFileExtension;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface ImageService {
    void saveImageFile(MultipartFile image, UUID uuid, ImageFileExtension fileExtension);
    void deleteImageFile(UUID uuid, ImageFileExtension imageFileExtension);
    Image getImageFile(UUID uuid, ImageFileExtension fileExtension);
}
