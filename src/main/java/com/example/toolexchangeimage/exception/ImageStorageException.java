package com.example.toolexchangeimage.exception;

public class ImageStorageException extends RuntimeException {
    public ImageStorageException(String message) {
        super(message);
    }

    public ImageStorageException(Throwable cause) {
        super(cause);
    }
}
