package com.example.toolexchangeimage.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Slf4j
public class MvcExceptionHandler {

    @ExceptionHandler(ImageNotFoundException.class)
    @ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Slika nije pronađena")
    public void imageNotFound(HttpServletRequest req, Exception e) {
      log.info("Image not found", e);
    }

    @ExceptionHandler(ImageStorageException.class)
    @ResponseStatus(value= HttpStatus.NOT_ACCEPTABLE, reason="Spremanje slike nije uspjelo")
    public void imageStorageFailed(HttpServletRequest req, Exception e) {
      log.info("Image storage failed", e);
    }
}
