package com.example.toolexchangeimage.controller;

import com.example.toolexchangeimage.model.Image;
import com.example.toolexchangeimage.model.ImageFileExtension;
import com.example.toolexchangeimage.service.ImageService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@RestController
@RequestMapping("image")
@CrossOrigin("*")
public class ImageController {
    private final ImageService imageService;

    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @PostMapping("{uuid}/{extension}")
    public ResponseEntity<Void> saveImages(@RequestParam("image") MultipartFile image, @PathVariable UUID uuid,
                            @PathVariable("extension") ImageFileExtension fileExtension) {
        this.imageService.saveImageFile(image, uuid, fileExtension);
        return ResponseEntity.accepted().build();
    }

    @DeleteMapping("{uuid}/{fileExtension}")
    public ResponseEntity<Void> deleteImage(@PathVariable UUID uuid, @PathVariable ImageFileExtension fileExtension) {
        this.imageService.deleteImageFile(uuid, fileExtension);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "{imageUuid}/{extension}",
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> getFile(@PathVariable UUID imageUuid,
                                          @PathVariable("extension") ImageFileExtension fileExtension) {
        Image image = this.imageService.getImageFile(imageUuid, fileExtension);

        HttpHeaders headers = new HttpHeaders();
        switch (fileExtension) {
            case JPEG:
            case JPG:
                headers.setContentType(MediaType.IMAGE_JPEG);
                break;
            case PNG:
                headers.setContentType(MediaType.IMAGE_PNG);
                break;
            default:
                throw new IllegalArgumentException();
        }

        return new ResponseEntity<>(image.getBytes(), headers, HttpStatus.OK);
    }
}
