package com.example.toolexchangeimage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ToolExchangeImageApplication {

//    public ToolExchangeImageApplication(@Value("${project.id}") String projectId,
//                                        @Value("${secret.location}") String secretLocation) {
//        this.projectId = projectId;
//        this.secretLocation = secretLocation;
//    }

    public static void main(String[] args) {
        SpringApplication.run(ToolExchangeImageApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

//    @Bean
//    @Profile("prod")
//    public Bucket bucket() throws IOException {
//        FileInputStream serviceAccount =
//                new FileInputStream(this.secretLocation);
//
//        FirebaseOptions options = FirebaseOptions.builder()
//                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
//                //.setCredentials(GoogleCredentials.getApplicationDefault())
//                .setStorageBucket(this.projectId + ".appspot.com")
//                .build();
//
//        FirebaseApp.initializeApp(options);
//
//        return StorageClient.getInstance().bucket();
//    }

}
