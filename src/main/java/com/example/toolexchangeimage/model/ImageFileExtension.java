package com.example.toolexchangeimage.model;

public enum ImageFileExtension {
    JPEG, JPG, PNG
}
