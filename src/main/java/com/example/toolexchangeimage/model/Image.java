package com.example.toolexchangeimage.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Locale;
import java.util.UUID;

@Data
public class Image {
    private UUID uuid;
    private ImageFileExtension imageFileExtension;
    private byte[] bytes;
    @JsonIgnore
    public String getImageFileName() {
        return this.uuid.toString() + "." + this.imageFileExtension.name().toLowerCase(Locale.ROOT);
    }
}
